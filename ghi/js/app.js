window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    let data;
    let incrementvalue = 0; /* this value was used to inctrement through the for loop to fix
         the issue of them being in one column i created three seperate columns and when i hit above
         two i cleared the increment and started over this causes it to dynamically sort each conference
         into three columns. */

    try {
        const response = await fetch(url);
        data = await response.json();

        if (response.ok) {
            for (let conference of data.conferences) {

                incrementvalue += 1; // adds one every time it passes through the loop
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDateInit = new Date(details.conference.starts);
                    const endDateInit = new Date(details.conference.ends);
                    const locat = details.conference.location.name;
                    const startDate = startDateInit.toLocaleDateString();
                    const endDate = endDateInit.toLocaleDateString();
                    const html = createCard(name, description, pictureUrl, startDate, endDate, locat);

                    // confernce in column sorting method
                    if (incrementvalue === 1) {
                        const confTag = document.getElementById('col1');
                        confTag.innerHTML += html;
                    } else if (incrementvalue === 2) {
                        const confTag = document.getElementById('col2');
                        confTag.innerHTML += html;
                    } else {
                        const confTag = document.getElementById('col3');
                        confTag.innerHTML += html;
                        incrementvalue = 0;
                    }
                };
            }
        } else {
            console.log("Bad Response detected")
        }

    } catch (e) {
        console.log("Something went wrong, js json failed");
        const errorHtml = document.getElementById('row1');
        errorHtml.innerHTML = `<div class='alert alert-danger'>
                                    Something went wrong!!! Conference's failed to load :(
                                </div>`;
    }

    // console.log(data)

});

function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
        <div class="shadow p-1 mt-3">
            <div class="card">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
                <div class="card-footer">
                    <b class="card-text">${startDate} -- ${endDate}</b>
                </div>
            </div>
        </div>


    `;
}
