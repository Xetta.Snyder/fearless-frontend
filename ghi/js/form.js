window.addEventListener('DOMContentLoaded', async () => {
    let confUrl = 'http://localhost:8000/api/conferences/'
    const locationUrl = 'http://localhost:8000/api/locations/'
    let data;

    try {

        const response = await fetch(locationUrl)
        if (response.ok) {
            let optTag = document.getElementById('location')

            data = await response.json();
            console.table(data.locations)
            for (let location of data.locations) {
                let option = document.createElement('option');
                option.value = location.id;
                option.innerHTML = location.name;
                optTag.appendChild(option);
            }
        }

    } catch {
        console.error("Something went wrong with form.js");
        const errorHtml = document.getElementById('buttonc');
        errorHtml.innerHTML = `<div class='alert alert-danger'> Something went wrong!!! </div>`
    }
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const jsonD = JSON.stringify(Object.fromEntries(formData));
        const fetchConfig = {
            method: "POST",
            body: jsonD,
            Headers:{
                'Content-Type': 'application/json'
            }
        };
        const response = await fetch(confUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);

        }

    })

});


