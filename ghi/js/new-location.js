window.addEventListener('DOMContentLoaded', async () => {
    const locationUrl = 'http://localhost:8000/api/locations/';
    const url = 'http://localhost:8000/api/states/'
    let data;
    let stateList = []

    try {
        const response = await fetch(url);
        if (response.ok) {
            let optTag = document.getElementById('state')

            data = await response.json();
            for (let state of data.states) {
                let option = document.createElement('option');
                option.value = state.state_abbreviation;
                option.innerHTML = state.state_name;
                optTag.appendChild(option);
            }
        }
    } catch {
        console.error("Something went wrong with new-location.js")
    }

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const jsonD = JSON.stringify(Object.fromEntries(formData));
        const fetchConfig = {
            method: "POST",
            body: jsonD,
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation)
        }
    })
});
