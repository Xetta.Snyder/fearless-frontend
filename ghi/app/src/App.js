import logo from './logo.svg';
import './App.css';
import Nav from './components/Nav';
import AttendeeList from './components/AttendeesList';
import LocationForm from './components/LocationForm';
import PresentationForm from './components/PresentationForm';
import ConferenceForm from './components/new-conference';
import AttendConferenceForm from './components/AttendConferenceForm';
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route } from 'react-router-dom'



function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  return (
    <>
      <BrowserRouter>
        <Nav />
        <div className="container">
          <Routes>
            <Route path='/home' index={true} element={<MainPage />} />
            <Route path='/locations/new' element={ <LocationForm /> } />
            <Route path='/attendees' element={ <AttendeeList attendees={props.attendees} /> } />
            <Route path='/conferences/new' element={ <ConferenceForm /> } />
            <Route path='/attendees/new' element={ <AttendConferenceForm /> } />
            <Route path='/presentation' element={ <PresentationForm />} />
          </Routes>
        </div>
      </BrowserRouter>
    </>
  )
}

export default App;
